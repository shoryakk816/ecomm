const app = require('./src/app');

const port = '5005';
app.listen(port, ()=>{
    console.log(`Listening on port: ${port}`)
})