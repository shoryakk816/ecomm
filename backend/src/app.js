const express = require('express');
const path = require('path')
const cors = require('cors')
// const cookieParser = require('cookie-parser')
const morganBody = require('morgan-body')
const bodyParser = require('body-parser')

const authenticationMiddleware = require('./middlewares/authentication')
const corsConfig = require('./configs/cors.config')
const loggerMiddleware = require('./middlewares/loggerMiddleware')
const requestMethodMiddleware = require('./middlewares/loggerMiddleware')
const secureRouter = require('./routers/secureRouter')

const app = express();

app.use(bodyParser.json({
    defaultCharset: 'utf-8'
  }))
  
app.use(express.json())
  
// app.set('trust proxy', 1)
app.use(cors(corsConfig))
    
app.use(loggerMiddleware)
app.use(requestMethodMiddleware)
morganBody(app)

app.use('/api/v1',
    authenticationMiddleware,
    secureRouter)

module.exports = app