const axios = require('axios')
const {
  BASE_URL
} = require('./common.config')

const apiClient = axios.create({
  baseURL: `${BASE_URL}`,
})

module.exports = apiClient;
