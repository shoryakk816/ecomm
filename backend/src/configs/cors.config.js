const { ALLOWED_ORIGIN } = require('./common.config')

const corsConfig = {
  credentials: true,
  methods: ['GET,POST'],
  origin: true,
}

module.exports = corsConfig
