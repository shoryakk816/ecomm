const fs = require("fs");
const { v4: uuidv4 } = require("uuid");

const PRODUCT_FILE_NAME = "src/resources/products.json";

const readProducts = () => {
  const imageData = fs.readFileSync(PRODUCT_FILE_NAME, "utf-8");
  return JSON.parse(imageData);
};

const generateCombinedUUID = () => {
  let fullUUID = uuidv4().replace(/-/g, "");
  let combinedUUID =
    fullUUID.substring(0, 5) +
    fullUUID.substring(12, 17) +
    fullUUID.substring(24, 32);

  return combinedUUID.substring(0, 16);
};

const writeFile = (data) => {
  try {
    fs.writeFileSync(PRODUCT_FILE_NAME, JSON.stringify(data, null, 4));
    console.log("File was written successfully");
  } catch (err) {
    console.error("Error writing file:", err);
  }
};

module.exports = {
  deaultRouter: (req, res) => {
    console.log("Calling default router...");
    return res.json({ message: "Hello World" });
  },

  fetchProducts: async (req, res) => {
    try {
      const { page, perPage } = req.body;

      const images = readProducts();
      const startIndex = page * perPage;

      return res.json({
        items: images.slice(startIndex, startIndex + perPage),
        page: page,
        perPage: perPage,
        total: images.length,
        totalPages: 2,
      });
    } catch (error) {
      return res.status(400).json({ message: error.message });
    }
  },

  addProduct: async (req, res) => {
    try {
      const id = generateCombinedUUID();
      console.log("id", id);
      const product = {
        id: id,
        ...req.body,
      };

      console.log("product", product);
      let products = readProducts();
      products.push(product);

      writeFile(products);
      return res.json({
        success: true,
        message: `Successfully added product: ${id}`,
      });
    } catch (error) {
      return res.status(400).json({ message: error.message });
    }
  },

  updateProduct: async (req, res) => {
    try {
      const { id } = req.body;

      let products = readProducts();
      products.forEach((product) => {
        if (product.id == id) {
          Object.assign(product, req.body);
        }
      });
      writeFile(products);

      return res.json({
        success: true,
        message: `Successfully updated product: ${id}`,
      });
    } catch (error) {
      return res.status(400).json({ message: error.message });
    }
  },

  deleteProduct: async (req, res) => {
    try {
      const { id } = req.body;

      let products = readProducts().filter((product) => product.id != id);
      writeFile(products);

      return res.json({
        success: true,
        message: `Successfully deleted product: ${id}`,
      });
    } catch (error) {
      return res.status(400).json({ message: error.message });
    }
  },
};
