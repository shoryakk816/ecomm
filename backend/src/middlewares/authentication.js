module.exports = async (req, res, next) => {
    // if (!req.headers.authorization.startsWith('Bearer ')) {
    //   return res.status(403).send('You must be logged in!')
    // }
  
    try {
    //   const idToken = req.headers.authorization.split('Bearer ')[1]
    //   req.currentUser = await firebaseAuth.verifyIdToken(idToken)
      return next()
    } catch (err) {
      return res.status(403).send('You must be logged in!')
    }
  }
  