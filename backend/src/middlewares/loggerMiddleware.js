module.exports = async (req, res, next) => {
    console.log(`Request URL: ${req.originalUrl}`)
    console.log(`Request Origin: ${req.headers.origin}`)
    console.log(`Referring Domain: ${req.headers.referer}`)
    return next()
  }
  