module.exports = (req, res, next) => {
    const allowedMethods = ['GET', 'POST', 'PUT', 'DELETE']
  
    if (!allowedMethods?.includes(req.method)) {
      res.status(405).send(`${req.method} not allowed.`)
    }
  
    next();
  }
  