const router = require("express").Router();
const controller = require("../controller");

router.get("/test", controller.deaultRouter);
router.post("/fetchProducts", controller.fetchProducts);

router.post("/products/add", controller.addProduct);
router.post("/products/update", controller.updateProduct);
router.post("/products/delete", controller.deleteProduct);

module.exports = router;
