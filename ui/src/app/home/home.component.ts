import { Component, ViewChild } from '@angular/core';
import { ProductService } from '../services/product.service';
import { ApiOutput, Product, Products } from '../../commons/types';
import { Route } from '../../commons/enums';
import { ProductComponent } from '../components/product/product.component';
import { CommonModule } from '@angular/common';
import { Paginator, PaginatorModule } from 'primeng/paginator';
import { EditPopupComponent } from '../components/edit-popup/edit-popup.component';
import { ButtonModule } from 'primeng/button';

@Component({
  selector: 'app-home',
  standalone: true,
  imports: [
    ProductComponent,
    CommonModule,
    PaginatorModule,
    EditPopupComponent,
    ButtonModule
  ],
  templateUrl: './home.component.html',
  styleUrl: './home.component.scss',
})
export class HomeComponent {
  @ViewChild('paginator') paginator: Paginator | undefined;

  products: Product[];
  totalRecords: number;
  rows: number;

  displayEditProduct: boolean;
  displayAddProduct: boolean;
  displayDeleteProduct: boolean;

  selectedProduct: Product = {
    id: '',
    price: '',
    name: '',
    image: '',
    rating: 0,
  };

  constructor(private prouctService: ProductService) {
    this.products = [];
    this.totalRecords = 0;
    this.rows = 5;
    this.displayAddProduct = false;
    this.displayDeleteProduct =false;
    this.displayEditProduct = false;
  }

  toggleEditPopup(product: Product) {
    this.selectedProduct = product;
    this.displayEditProduct = !this.displayEditProduct;
  }

  toggleAddPopup() {
    console.log("Toggling add: ", this.displayAddProduct);
    this.displayAddProduct = !this.displayAddProduct;
  }

  toggleDeletePopup(product: Product) {
    this.selectedProduct = product;
    this.displayDeleteProduct = !this.displayDeleteProduct;
  }

  onPageChange(event: any) {
    console.log({ event });
    this.fetchProducts(event.page, event.rows);
  }

  resetPaginator() {
    this.paginator?.changePage(0);
  }

  fetchProducts(page: number, perPage: number) {
    this.prouctService
      .getProducts(Route.fetchProducts, {
        page: page,
        perPage: perPage,
      })
      .subscribe((products: Products) => {
        console.log({ products });
        this.products = products.items;
        this.totalRecords = products.total;
      });
  }

  updateProduct(payload: Product) {
    this.prouctService.updateProduct(Route.updateProduct, payload).subscribe({
      next: (out: ApiOutput) => {
        console.log('Update output: ', out);
        this.fetchProducts(0, this.rows);
        this.resetPaginator();
      },
      error: (error) => {
        console.log('Update output: ', error);
      },
    });
  }

  addProduct(payload: Product) {
    this.prouctService.updateProduct(Route.addProduct, payload).subscribe({
      next: (out: ApiOutput) => {
        console.log('Add output: ', out);
        this.fetchProducts(0, this.rows);
        this.resetPaginator();
      },
      error: (error) => {
        console.log('Add output: ', error);
      },
    });
  }

  deleteProduct(payload: Product) {
    this.prouctService.updateProduct(Route.deleteProduct, payload).subscribe({
      next: (out: ApiOutput) => {
        console.log('Delete output: ', out);
        this.fetchProducts(0, this.rows);
        this.resetPaginator();
      },
      error: (error) => {
        console.log('Delete output: ', error);
      },
    });
  }

  onConfirmEdit(payload: Product) {
    this.updateProduct(payload);
    this.displayEditProduct = !this.displayEditProduct;
  }

  onConfirmAdd(payload: Product) {
    console.log("Adding", payload);
    this.addProduct(payload);
    this.displayAddProduct = !this.displayAddProduct;
  }

  onConfirmDelete(payload: Product) {
    this.deleteProduct(payload);
    this.displayDeleteProduct = !this.displayDeleteProduct;
  }

  // Runs on init of the class
  ngOnInit() {
    this.fetchProducts(0, this.rows);
  }
}
