import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpOptions } from '../../commons/types';

@Injectable({
  providedIn: 'root',
})
export class ApiService {
  private baseUrl: string;

  constructor(private httpClient: HttpClient) {
    this.baseUrl = 'http://localhost:5005/api/v1/';
  }

  get<T>(endPoint: string, options: HttpOptions): Observable<T> {
    return this.httpClient.get<T>(
      `${this.baseUrl}${endPoint}`,
      options
    ) as Observable<T>;
  }

  post<T>(
    endPoint: string,
    options: HttpOptions,
    payload?: any
  ): Observable<T> {
    return this.httpClient.post<T>(
      `${this.baseUrl}${endPoint}`,
      payload,
      options
    ) as Observable<T>;
  }
}
