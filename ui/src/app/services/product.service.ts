import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import {
  ApiOutput,
  PaginationParams,
  Product,
  Products,
} from '../../commons/types';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ProductService {
  constructor(private apiService: ApiService) {}

  // fetch list of products from backend
  getProducts = (
    endPoint: string,
    payload: PaginationParams
  ): Observable<Products> => {
    return this.apiService.post(
      endPoint,
      {
        responseType: 'json',
      },
      payload
    );
  };

  // Update products
  // Delete products
  // Add product

  updateProduct = (
    endPoint: string,
    payload: Product
  ): Observable<ApiOutput> => {
    return this.apiService.post(
      endPoint,
      {
        responseType: 'json',
      },
      payload
    );
  };
}
