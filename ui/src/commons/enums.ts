export enum Route {
  fetchProducts = 'fetchProducts',
  updateProduct = 'products/update',
  addProduct = 'products/add',
  deleteProduct = 'products/delete',
}

export enum Theme {
  LIGHT = 'light',
  DARK = 'dark',
}
